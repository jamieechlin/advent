const EMPTY = 'L'
const OCCUPIED = '#'

const fs = require('fs')
const path = require('path')
const deepEqual = require('deep-equal')

const text = fs.readFileSync(path.join(__dirname, './sample.txt'), 'utf8')
let data = text.split(/\n/).map(x => x.split(''))

const update = (seats) => {
    return seats.map((row, rowIdx) => row.map((seat, colIdx) => {
        let adjacentOccupiedSeats = countOccupiedSeatsInLineOfSight(seats, rowIdx, colIdx);
        if (seat === EMPTY && adjacentOccupiedSeats === 0) {
            return OCCUPIED
        }
        if (seat === OCCUPIED && adjacentOccupiedSeats >= 5) {
            return EMPTY
        }
        return seat
    }))
}

const range = (size, startAt = 0) => [...Array(size).keys()].map(i => i + startAt)

const countOccupiedSeatsInLineOfSight = (seats, rowIdx, colIdx) => {
    let retVal = 0

    retVal += range(seats.length).some(r => r !== rowIdx && seats[r][colIdx] === OCCUPIED) ? 1 : 0
    retVal += range(seats[0].length).some(c => c !== colIdx && seats[rowIdx][c] === OCCUPIED)  ? 1 : 0

    // for (let r = Math.max(rowIdx - 1, 0); r <= Math.min(rowIdx + 1, seats.length - 1); r++) {
    //     for (let c = colIdx - 1; c <= colIdx + 1; c++) {
    //         if (seats[r][c] === OCCUPIED && !(r === rowIdx && c === colIdx)) {
    //             retVal++
    //         }
    //     }
    // }

    return retVal
}

// 3, 1 => 2, 0


const countOccupiedSeatsInRow = (row) => row.reduce((init, c) => init + (c === OCCUPIED ? 1 : 0), 0)

const countOccupiedSeats = (seats) => seats.reduce((init, row) => init + countOccupiedSeatsInRow(row), 0)

const transform = () => {
    let original
    let counter = 0
    do {
        original = data.map(r => [...r])
        data = update(data)
        console.log(counter++)
    } while (!deepEqual(original, data))

    console.log(countOccupiedSeats(data))
}

const sum = (a, b) => a + b;
module.exports = {sum, countOccupiedSeatsInLineOfSight};
