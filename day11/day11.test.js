const {countOccupiedSeatsInLineOfSight} = require("./main2");
const {sum} = require('./main2')
const deepEqual = require('deep-equal')

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
});

test('countOccupiedSeatsInLineOfSight', () => {
    const input = [
        ['#', '.', '#'],
        ['.', ".", '.'],
        ['#', "#", '#'],
        ['.', ".", '.'],
        ['#', "#", '#']
    ]

    expect(countOccupiedSeatsInLineOfSight(input, 0, 2)).toBe(3)
    expect(countOccupiedSeatsInLineOfSight(input, 0, 1)).toBe(3)
})

const diagonalSlice = (array, row, col, rowIncr, colIncr) => {
    const retVal = []

    let target
    do {
        row += rowIncr
        col += colIncr

        target = array[row] ? array[row][col] : undefined;
        if (!!target) {
            retVal.push(target)
        }
    } while (!!target)

    return retVal
}


test('tests diagonal', () => {
    const input = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8]
    ];
    // console.log(diagonal(input, false))
    console.log(diagonalSlice(input, 0, 0, 0, 1))
    console.log(diagonalSlice(input, 0, 0, -1, 0))
})


test('arrays equals', () => {
    console.log(deepEqual([0], [0]))
})