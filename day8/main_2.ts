import * as path from "path";
import * as fs from "fs";

const data = fs.readFileSync(path.join(__dirname, 'data.txt'), 'utf-8')

const instructions = data.split(/\n/)
const changedLines: number[] = []

const execute = (): number => {

    const executedLines: number[] = []
    let acc = 0
    let currentLine = 0
    let alreadyMangled = false

    const processInstruction = (line: string) => {
        let [instr, valueStr] = line.split(/ /)
        let value = parseInt(valueStr)

        if (!alreadyMangled && !changedLines.includes(currentLine) && ['jmp', 'nop'].includes(instr)) {
            alreadyMangled = true
            changedLines.push(currentLine)
            instr = instr === 'jmp' ? 'nop' : 'jmp'
        }

        switch (instr) {
            case 'nop':
                currentLine++
                break
            case 'acc':
                acc += value
                currentLine++
                break
            case 'jmp':
                currentLine += value
                break;
        }
    }

    while (!executedLines.includes(currentLine)) {
        executedLines.push(currentLine)
        processInstruction(instructions[currentLine])
        if (currentLine >= instructions.length) {
            console.log('Finished', acc)
            process.exit(0)
        }
    }

    return acc
}

while (true) {
    execute()
}
