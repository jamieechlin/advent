import * as path from "path";
import * as fs from "fs";

const data = fs.readFileSync(path.join(__dirname, 'data.txt'), 'utf-8')

const instructions = data.split(/\n/)

const executedLines = []
let acc = 0
let currentLine = 0

const processInstruction = (line: string) => {
    const [instr, valueStr] = line.split(/ /)
    let value = parseInt(valueStr)

    switch (instr) {
        case 'nop':
            currentLine++
            break
        case 'acc':
            acc += value
            currentLine++
            break
        case 'jmp':
            currentLine += value
            break;
    }
}

while (!executedLines.includes(currentLine)) {
    executedLines.push(currentLine)
    processInstruction(instructions[currentLine])
}

console.log(acc)
